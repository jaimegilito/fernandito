/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fernandito;

/**
 *
 * @author jaime
 */
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
      
public class Fernandito {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         try{
            FileWriter archivo = new FileWriter("quijote.txt");
            BufferedWriter bw =  new BufferedWriter (archivo);
            PrintWriter salida = new PrintWriter (bw);
                salida.println("En un lugar de la Mancha, de cuyo nombre no quiero acordarme, no ha"
                        + " \nmucho tiempo que vivía un hidalgo de los de lanza en astillero, adarga"
                        + " \nantigua, rocín flaco y galgo corredor. Una olla de algo más vaca que carnero,"
                        + " \nsalpicón las más noches, duelos y quebrantos los sábados, lantejas los"
                        + " \nviernes, algún palomino de añadidura los domingos8, consumían las tres"
                        + " \npartes de su hacienda. El resto della concluían sayo de velarte, calzas de"
                        + " \nvelludo para las fiestas, con sus pantuflos de lo mesmo, y los días de"
                        + " \nentresemana se honraba con su vellorí de lo más fino. Tenía en su casa una"
                        + " \nama que pasaba de los cuarenta, y una sobrina que no llegaba a los veinte,"
                        + " \ny un mozo de campo y plaza, que así ensillaba el rocín como tomaba la"
                        + " \npodadera. Frisaba la edad de nuestro hidalgo con los cincuenta años; era de"
                        + " \ncomplexión recia, seco de carnes, enjuto de rostro, gran madrugador y"
                        + " \namigo de la caza. Quieren decir que tenía el sobrenombre de Quijada, o"
                        + " \nQuesada, que en esto hay alguna diferencia en los autores que deste caso"
                        + " \nescriben; aunque, por conjeturas verosímiles, se deja entender que se"
                        + " \nllamaba Quejana. Pero esto importa poco a nuestro cuento; basta que en la"
                        + " \nnarración dél no se salga un punto de la verdad.");
                       
                 salida.close();
                 bw.close();
           
        }catch (IOException e){
            System.err.println("ERROR " + e );
        }
    }
    
}
